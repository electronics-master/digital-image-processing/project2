import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()

# import some common libraries
import numpy as np

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg

import sys; sys.path.insert(1, "detectron2/projects/PointRend")
import point_rend

class Segmentation:

    def __init__(self):
        # initialize a configuration
        cfg = get_cfg()

        point_rend.add_pointrend_config(cfg)
        
        # add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library
        #cfg.merge_from_file('configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml')
        cfg.merge_from_file('detectron2/projects/PointRend/configs/InstanceSegmentation/pointrend_rcnn_R_50_FPN_3x_coco.yaml')

        # use cpu
        cfg.MODEL.DEVICE = 'cpu'
        # set threshold for this model
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  
        # find a model from detectron2's model zoo.
        #cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
        cfg.MODEL.WEIGHTS = "https://dl.fbaipublicfiles.com/detectron2/PointRend/InstanceSegmentation/pointrend_rcnn_R_50_FPN_3x_coco/164955410/model_final_3c3198.pkl"

        # finally, create the predictor
        self.predictor = DefaultPredictor(cfg)

    def process(self, image):
        outputs = self.predictor(image)
        
        tensor = outputs['instances'].to('cpu').pred_masks[0]
        mask = np.float32(tensor.numpy())

        return mask
