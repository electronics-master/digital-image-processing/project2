#!/usr/bin/env python3

import numpy as np
import cv2
from segmentation import Segmentation

def main():
    segmenter = Segmentation()

    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH,240);
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT,240);

    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        mask = segmenter.process(frame)

        # Display the resulting frame
        cv2.imshow('frame',mask)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
