# Detectron2 Based Segmentation

The provided segmenter is based on Facebook's Detectron2 Deep Learning
framwork. It's extremely slow, but performance is not important for
this project.

## Installing the Dependencies

Make sure the following dependencies are installed:

```bash
# Pytorch and OpenCV
sudo -H pip3 install torch torchvision opencv-python

# Facebook's utilities
sudo -H pip3 install 'git+https://github.com/facebookresearch/fvcore'

# Install our local copy of detectron2
sudo -H pip3 install -e detectron2
```

## Running the Example

You'll find a simple example that shows how to segment skin in a
scene. The example will attempt to open the camera and display the
resulting mask in a window.

To run the example type:

```bash
python3 ./proyecto2.py
```